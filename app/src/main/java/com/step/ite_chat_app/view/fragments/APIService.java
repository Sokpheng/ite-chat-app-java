package com.step.ite_chat_app.view.fragments;

import com.step.ite_chat_app.services.notifications.MyResponse;
import com.step.ite_chat_app.services.notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAYwlQAWA:APA91bGHUCkXP4QOyhLPJwjJPlkbecmukipFFKdGNkq2QBlvUjkBQ3YVELiMyUZV3eGF6zowIULSnY_c3udVgaT2OHtEA5mmiD0IAOJin6rO3f2ULu58VF1Lb08VoUM-I4xo1GxTI3dd"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}

